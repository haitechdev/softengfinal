﻿using Microsoft.AspNetCore.Server.Kestrel.Http;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lunch2GoAPI.Models
{
    public class DB
    {
        private static string MyConnection = "server=hp169.hostpapa.com; uid=haite237_food2go;pwd=haitana12;database=haite237_foodtogo;";
        //This is  MySqlConnection here i have created the object and pass my connection string.  
        MySqlConnection conn = new MySqlConnection(MyConnection);

        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                conn.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }

        //Close connection
        private void CloseConnection()
        {
            try
            {
                conn.Close();
            }
            catch
            {

            }
        }

        //Insert statement
        public string Insert(dynamic value)
        {
            //Attempts to connect to database
            bool connectionstatus = OpenConnection();

            
            
            if(connectionstatus == true)
            {
                string option = value.Option;
                string addon = value.AddOn;
                string delivery = value.Delivery;
                string streetaddress = value.StreetAddress;
                string towncity = value.TownCity;
                string region = value.Region;
                string cost = value.Cost;
                string customer = value.Customer;
                DateTime date = value.DateTime;

                //This Insert statement inserts the new order into the database  
                string Query = "INSERT INTO `haite237_foodtogo`.`orders` (`id`, `option`, `addon`, `delivery`, `streetaddress`, `towncity`, `region`, `cost`, `customer`, `datetime`) VALUES (NULL, '" + option + "', '" + addon + "', '" + delivery + "', '" + streetaddress + "', '" + towncity + "', '" + region + "', '" + cost + "', '" + customer + "', '" + date + "');";

                //This is command class which will handle the query and connection object.  
                MySqlCommand MyCommand = new MySqlCommand(Query, conn);
                MySqlDataReader MyReader;
                //This command executes the query onto the database
                MyReader = MyCommand.ExecuteReader();
                CloseConnection();
                return "true";
            }
            else
            {
                return "false";
            }            
        }
    }
}