﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using MySql.Data.MySqlClient;
using Lunch2GoAPI.Models;
using System.Collections;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class InsertController : Controller
    {
        // GET api/insert
        [HttpGet]
        public string Get()
        {
            return "This is the Food to Go Insert API";
        }

        // POST api/insert
        //This Method is used to receive HttpPost requests
        //When a request is received the Insert method in the DB class is called and the value parameter is passed through
        [HttpPost]
        public string Post([FromBody]dynamic value)
        {
            DB db = new DB();

            return db.Insert(value);
        }
    }
}