﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using D321_A2_Haitana;
using D321_A2_Haitana.Services;
using Windows.Networking.Connectivity;
using System.Linq;
using Windows.Storage;
using System.IO;
using SQLite.Net;
using D321_A2_Haitana.Models;
using System.Threading.Tasks;

namespace FoodToGoAppUnitTesting
{
    [TestClass]
    public class FoodToGoTests
    {
        public string password = "123";
        public static string confirmpassword = "123";
        public string email = "mhaitana@gmail.com";          
        public string creditcardnumber = "5191630128456037";
        public string cvcnumber = "123";
        public int checkordertimeresult = -1;
        

        [TestMethod]
        public void CheckInternetStatus()
        {
            bool result;

            var profile = NetworkInformation.GetInternetConnectionProfile();
            if (profile != null && profile.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess)
            {
                result = true;
            }
            else
            {
                result = false;
            }

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CreditCardNoCheck()
        {
            D321_A2_Haitana.Services.Validation v = new D321_A2_Haitana.Services.Validation();
            v.CreditCardValidator();
            bool result = v.HasCreditCardNumber(creditcardnumber);

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CheckEmailAddress()
        {
            D321_A2_Haitana.Services.Validation v = new D321_A2_Haitana.Services.Validation();
            bool result = v.VerfiyEmail(email);

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CVCNoCheckIsInteger()
        {
            bool result;
            D321_A2_Haitana.Services.Validation v = new D321_A2_Haitana.Services.Validation();

            result = v.CheckCVC(cvcnumber);

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void ConfirmPasswords()
        {
            bool result;

            if (password == confirmpassword)
            {
                result = true;
            }
            else
            {
                result = false;
            }

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CheckOrderTime()
        {
            D321_A2_Haitana.Services.Validation v = new D321_A2_Haitana.Services.Validation();
            int result = v.CheckOrderTime();           

            Assert.AreEqual(checkordertimeresult, result);
        }

    }
}
