﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using D321_A2_Haitana;
using System.IO;
using Windows.Storage;
using SQLite.Net;

namespace D321_A2_Haitana.Services
{
    //The CheckLoginDetails class is used to verify a user when they login
    public class CheckLoginDetails
    {
        /// <summary>
        /// The CheckUserDetails method is used to verify the user when they log in
        /// A select statement is sent to the SQLite database to see if there is a record with the same username and password that
        /// was entered. The result is sent to var u. Is the details were correct then var u will not equal null therefore App.user is assignment with the record loaded
        /// in var u and the savedUser method is called sending the username.
        /// </summary>
        /// <param name="uname">The username the user has enter</param>
        /// <param name="pin">The pin/password the user entered</param>
        /// <returns>Returns the status of the login - True = successful login, False = Unsuccessful login</returns>
        public bool CheckUserDetails(string uname, string pin)
        {
            var u = App.conn.Query<Customers>("select * from Customers where Username = ? and Pin = ?", uname, pin).FirstOrDefault();

            if(u != null)
            {
                App.user = u;                
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// The username is saved to the loggedIn.txt file
        /// </summary>
        /// <param name="uname">The username of the user logged in. If the user is logging out then "" is sent as the parameter value</param>
        public async void savedUser(string uname)
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile sampleFile = await storageFolder.GetFileAsync("loggedIn.txt");

            await FileIO.WriteTextAsync(sampleFile, uname);
        }
    }
}