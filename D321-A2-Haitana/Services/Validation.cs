﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace D321_A2_Haitana.Services
{
    
    public class Validation
    {
        //This Method is used to verify the users credit card input
        public void CreditCardValidator()
        {
            //These are the regex for all the available Credit cards
            this.Patterns = new string[] { "^4[0-9]{12}(?:[0-9]{3})?$", "^5[1-5][0-9]{14}$", "^3[47][0-9]{13}$", "^3(?:0[0-5]|[68][0-9])[0-9]{11}$", "^6(?:011|5[0-9]{2})[0-9]{12}$", "^(?:2131|1800|35\\d{3})\\d{11}$" };
        }
        public string[] Patterns { get; set; }
        public bool HasCreditCardNumber(string input)
        {
            foreach (var pattern in Patterns)
            {
                if (Regex.IsMatch(input, pattern, RegexOptions.Multiline))
                {
                    return true;
                }
            }

            return false;
        }

        //This method uses a Regex to verify the email address entered by the user
        public bool VerfiyEmail(string email)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (match.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //This method ensure the user entered a 3 digit CVC number
        public bool CheckCVC(string cvc)
        {
            if (Math.Floor(Math.Log10(Convert.ToInt32(cvc)) + 1) == 3)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //This method checks to see if the user is trying to make an order before the order cutoff time
        public int CheckOrderTime()
        {
            App.order.DateTime = DateTime.Now;
            DateTime orderCutOff = new DateTime(App.order.DateTime.Year, App.order.DateTime.Month,
                App.order.DateTime.Day, 10, 30, 00);

            int canOrder = DateTime.Compare(App.order.DateTime, orderCutOff);

            return canOrder;
        }
    }
}
