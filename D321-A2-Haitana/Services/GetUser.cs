﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.System;

namespace D321_A2_Haitana.Services
{
    public class GetUser
    {
        /// <summary>
        /// This method is used to retrieved the details of the loggedin user when they reopen the app.
        /// </summary>
        /// <param name="uname">the username of the already logged in user</param>
        /// <returns>The Customers object returned from the database</returns>
        public static Customers RetrieveUser(string uname)
        {
            var u = App.conn.Query<Customers>("select * from Customers where Username = ?", uname).FirstOrDefault();

            return u;
        }
    }
}
