﻿using D321_A2_Haitana.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.System;
using Windows.UI.Xaml.Controls;

namespace D321_A2_Haitana.Services
{
    public class DeleteCustomer
    {
        /// <summary>
        /// This method is called when the user wishes to delete their account
        /// The SetBusy Methods are called with the text explaining what is happening with Delays to allow them time to read it
        /// The users details are deleted from the Customers table and all orders associated to that user are deleted from the Orders table
        /// The loggedIn.txt is overwritten with and empty string
        /// All objects are reinitialized
        /// Finally the login page is navigated too
        /// </summary>
        /// <param name="uname">The username of the logged in user</param>
        /// <param name="f">The frame object of the account page. Used to navigate to the login page</param>
        public async void DeleteUser(string uname, Frame f)
        {
            Busy.SetBusy(true, "Please wait while your account is deleted...");
            await Task.Delay(1000);

            Busy.SetBusy(true, "Deleting User Details...");
            App.conn.Query<Customers>("DELETE FROM Customers WHERE Username = ?", App.user.Username);
            await Task.Delay(1000);

            Busy.SetBusy(true, "Deleting Current Orders...");
            await Task.Delay(1000);

            Busy.SetBusy(true, "Deleting Past Orders...");
            await Task.Delay(1000);

            App.conn.Query<Orders>("DELETE FROM Orders WHERE Customer = ?", App.user.Username);

            CheckLoginDetails chk = new CheckLoginDetails();
            chk.savedUser("");

            App.user = new Customers();
            App.order = new Orders();
            App.orders = new List<Orders>();

            Busy.SetBusy(true, "Going back to login page...");
            await Task.Delay(1000);
            Busy.SetBusy(false);

            f.Navigate(typeof(LoginPage));
        }
    }
}
