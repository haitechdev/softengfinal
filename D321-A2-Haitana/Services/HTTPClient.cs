﻿using D321_A2_Haitana.Views;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace D321_A2_Haitana.Services
{
    //The HTTPClient Class used to conduct HttpClient tasks
    public class HTTPClient
    {
        //Insert Order function - Receives an order object then turns the object into a dynamic variable to send to the API over HttpPost
        public async Task InsertOrder(Orders value)
        {
            Uri requestUri = new Uri("http://localhost:2451/api/insert/");
            dynamic dynamicJson = new ExpandoObject();
            dynamicJson.Option = value.Option;
            dynamicJson.AddOn = value.AddOn;
            dynamicJson.Delivery = value.Delivery;
            dynamicJson.StreetAddress = value.StreetAddress;
            dynamicJson.TownCity = value.TownCity;
            dynamicJson.Region = value.Region;
            dynamicJson.DateTime = value.DateTime;
            dynamicJson.Cost = value.Cost;
            dynamicJson.Customer = value.Customer;
            dynamicJson.Status = value.Status;
            string json = "";
            json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
            var objClint = new HttpClient();
            var respon = await objClint.PostAsync(requestUri, new StringContent(json, System.Text.Encoding.UTF8, "application/json"));
            string responJsonText = await respon.Content.ReadAsStringAsync();

            if (responJsonText == "true")
            {
                App.OrderReceieved = true;          
                Busy.SetBusy(true, "Order sent successfully!");
                await Task.Delay(1000);
                Busy.SetBusy(false);
            }
            else
            {
                App.OrderReceieved = false;
                Busy.SetBusy(true, "Could not connect with Food to Go's ordering system...");
                await Task.Delay(1500);
                Busy.SetBusy(true, "Order has been saved locally...");
                await Task.Delay(1000);
                Busy.SetBusy(false);
            }
        }
    }
}
