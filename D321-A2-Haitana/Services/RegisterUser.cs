﻿using D321_A2_Haitana;
using D321_A2_Haitana.Services;
using D321_A2_Haitana.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace D321_A2_Haitana.Models
{
    public class RegisterUser
    {
        //This is the register user method. In receives all of the details entered my the user and conducts validation on the inputs. If all the data entered is valid then the user is saved into the SQLite database, 
        //Otherwise an error message with all the errors is returned
        public async Task<string> Register(string uname, string fname, string lname, string email, string address, string tc, string region, string ph, string pwd, string cpwd, string ccno, string ccm, string ccy, string ccv)
        {
            bool status = true;
            Services.Validation v = new Services.Validation();

            if (pwd == cpwd)
            {
                var query = App.conn.Table<Customers>();
                bool unameUsed = false;

                foreach (var user in query)
                {
                    if (user.Username == uname)
                    {
                        unameUsed = true;
                    }
                }

                if (unameUsed != true)
                {
                    App.user.Username = uname;
                    App.user.FirstName = fname;
                    App.user.LastName = lname;

                    bool ValidEmail = v.VerfiyEmail(email);

                    if(ValidEmail == true)
                    {
                        App.user.Email = email;
                    }
                    else
                    {
                        App.ErrorMessage = App.ErrorMessage + "Invalid Email Address!\n";
                        status = false;
                    }
                    
                    App.user.StreetAddress = address;
                    App.user.TownCity = tc;
                    App.user.Region = region;
                    try
                    {
                        App.user.Phone = Convert.ToInt32(ph);
                    }
                    catch
                    {
                        App.ErrorMessage = App.ErrorMessage + "Invalid Phone Number!\n";
                        status = false;
                    }

                    App.user.Pin = pwd;


                    
                    v.CreditCardValidator();
                    bool result = v.HasCreditCardNumber(ccno);
                    if (result == true)
                    {
                        App.user.CreditCardNo = Convert.ToInt64(ccno);
                    }
                    else
                    {
                        App.ErrorMessage = App.ErrorMessage + "Invalid Credit Card Number!\n";
                        status = false;
                    }

                    try
                    {
                        if(ccv.Length == 3)
                        {
                            App.user.CreditCardCvc = Convert.ToInt32(ccv);
                        }
                        else
                        {
                            App.ErrorMessage = App.ErrorMessage + "Invalid CVC Number!";
                        }
                    }
                    catch
                    {
                        App.ErrorMessage = App.ErrorMessage + "Invalid CVC Number!";
                        status = false;
                    }
                }
                else
                {
                    App.ErrorMessage = App.ErrorMessage + "Username already used!\n";
                    status = false;
                }
            }
            else
            {
                App.ErrorMessage = "Pins don't match!";
                status = false;
            }

            if (status == true)
            {
                Busy.SetBusy(true, "Registering Details...");

                var s = App.conn.Insert(new Customers()
                {
                    Username = App.user.Username,
                    FirstName = App.user.FirstName,
                    LastName = App.user.LastName,
                    Email = App.user.Email,
                    StreetAddress = App.user.StreetAddress,
                    TownCity = App.user.TownCity,
                    Region = App.user.Region,
                    RegionID = App.user.RegionID,
                    Phone = App.user.Phone,
                    Pin = App.user.Pin,
                    CreditCardNo = App.user.CreditCardNo,
                    CreditCardExpM = ccm,
                    CreditCardExpY = ccy,
                    CreditCardCvc = App.user.CreditCardCvc
                });

                await Task.Delay(1500);
                Busy.SetBusy(false);

                App.ErrorMessage = "";
                return "";
            }
            else
            {
                return "false";
            }
        }
    }
}
