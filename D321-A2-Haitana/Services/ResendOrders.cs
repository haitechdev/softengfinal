﻿using D321_A2_Haitana.Services;
using D321_A2_Haitana.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace D321_A2_Haitana.Services
{
    class ResendOrders
    {
        //This method retrieves all the loged in users unsent orders from the SQLite and attempts to resend them to the Food to Go Ordering Sysytem
        //It checks that the time is still before the order cutoff time
        //Displays the Busy Modal informing the user what is happening 
        public async Task AllLocalOrders(string uname)
        {
            var o = App.conn.Query<Orders>("select * from Orders where Customer = ? and Received = 0 and Status = 0", uname);

            App.order.DateTime = DateTime.Now;
            DateTime orderCutOff = new DateTime(App.order.DateTime.Year, App.order.DateTime.Month,
                App.order.DateTime.Day, 11, 00, 00);

            int canOrder = DateTime.Compare(App.order.DateTime, orderCutOff);

            if (o.Count > 0)
            {
                Busy.SetBusy(true, "Resending " + o.Count.ToString() + " orders...");
                await Task.Delay(1000);

                if (canOrder < 0)
                {                 
                    int i = 0;                    

                    foreach (Orders order in o)
                    {
                        i++;

                        Busy.SetBusy(true, "Resending " + i + "/" + o.Count.ToString() + " orders...");
                        await Task.Delay(500);

                        HTTPClient post = new HTTPClient();
                        await post.InsertOrder(order);

                        if (App.OrderReceieved == true)
                        {
                            var selectedOrder = App.conn.Query<Orders>("select * from Orders where Id = ?", order.Id).FirstOrDefault();

                            if (selectedOrder != null)
                            {
                                selectedOrder.Received = true;
                                App.conn.RunInTransaction(() =>
                                {
                                    App.conn.Update(selectedOrder);
                                });
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    Busy.SetBusy(true, "Order/s cannot be sent... Order time must be before 11:00am...");
                    await Task.Delay(1500);
                    Busy.SetBusy(false);
                }
                
            }
        }

        public async Task SingleLocalOrder(Orders order)
        {
            App.order.DateTime = DateTime.Now;
            DateTime orderCutOff = new DateTime(App.order.DateTime.Year, App.order.DateTime.Month,
                App.order.DateTime.Day, 11, 00, 00);

            int canOrder = DateTime.Compare(App.order.DateTime, orderCutOff);

            Busy.SetBusy(true, "Resending 1 order...");
            await Task.Delay(1000);

            if (canOrder > 0)
            {
                HTTPClient post = new HTTPClient();
                await post.InsertOrder(order);

                if (App.OrderReceieved == true)
                {
                    var selectedOrder = App.conn.Query<Orders>("select * from Orders where Id = ?", order.Id).FirstOrDefault();

                    if (selectedOrder != null)
                    {
                        selectedOrder.Received = true;
                        App.conn.RunInTransaction(() =>
                        {
                            App.conn.Update(selectedOrder);
                        });
                    }
                }
            }
            else
            {
                Busy.SetBusy(true, "Order cannot be sent... Order time must be before 11:00am...");
                await Task.Delay(1500);
                Busy.SetBusy(false);
            }
        }
    }
}
