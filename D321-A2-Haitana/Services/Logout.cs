﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template10.Services.NavigationService;
using Windows.UI.Xaml.Navigation;
using D321_A2_Haitana.Views;
using Windows.UI.Xaml.Controls;
using Template10.Mvvm;
using Windows.Storage;

namespace D321_A2_Haitana.Services
{
    public class Logout : ViewModelBase
    {
        /// <summary>
        /// This method is called when the user wishes to logout of their account
        /// The SetBusy Methods are called with the text explaining what is happening with Delays to allow them time to read it
        /// The loggedIn.txt is overwritten with and empty string
        /// All objects are reinitialized
        /// Finally the login page is navigated too
        /// </summary>
        /// <param name="f">The frame object of the page the logout method was called from. Used to navigate to the login page</param>
        public async void logout(Frame f)
        {          
            Busy.SetBusy(true, "Please wait while we log you out...");                       
            await Task.Delay(1000);

            Busy.SetBusy(true, "Clearing order details...");
            App.order = new Orders();
            App.orders = new List<Orders>();
            await Task.Delay(1000);

            Busy.SetBusy(true, "Clearing user details...");
            App.user = new Customers();

            CheckLoginDetails ckl = new CheckLoginDetails();
            ckl.savedUser("");

            await Task.Delay(1200);

            Busy.SetBusy(false);

            f.Navigate(typeof(LoginPage));
        }
    }
}
