﻿using System;
using Windows.Networking.Connectivity;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace D321_A2_Haitana.Services
{
    //This calss is used to detect whether this internet and to detect any changes in the network connection
    public class NetworkConnectionTrigger : StateTriggerBase
    {
        public bool requiresInternet;
        public NetworkConnectionTrigger()
        {
            NetworkInformation.NetworkStatusChanged += NetworkInformationOnNetworkStatusChanged;
        }

        public bool RequiresInternet
        {
            get { return this.requiresInternet; }
            set
            {
                requiresInternet = value;
                var profile = NetworkInformation.GetInternetConnectionProfile();
                if(profile != null && profile.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess)
                {
                    SetActive(value);
                }
                else
                {
                    SetActive(!value);
                }
            }
        }
        //This method detects for any change to the network status
        public async void NetworkInformationOnNetworkStatusChanged(object sender)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (NetworkInformation.GetInternetConnectionProfile() != null)
                {
                    SetActive(this.RequiresInternet);
                }
                else
                {
                    SetActive(!this.RequiresInternet);
                }
            });
        }
        //This method returns the status of the internet
        public bool getStatus()
        {
            var profile = NetworkInformation.GetInternetConnectionProfile();
            if (profile != null && profile.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
