﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.UI.Xaml.Controls;

namespace D321_A2_Haitana.Services
{
    public class DialogClass
    {
        /// <summary>
        /// This Method is to show the either the logout or exit confirmation dialog box when the user click logout or exit from any page
        /// The switch statement compares the Tag from the AppbarButton clicked to determine whether it needs to logout or exit the application
        /// </summary>
        /// <param name="b">The object of the AppBarbutton clicked - used the get the Tag used in the Switch statement</param>
        /// <param name="f">The frame object of the page the logout method was called from. Used to navigate to the login page</param>
        public async void ShowDialog(AppBarButton b, Frame f)
        {
            switch (b.Tag.ToString())
            {
                case "Logout":
                    var LogoutDialog = new Windows.UI.Popups.MessageDialog("Are you sure you want to logout?");

                    LogoutDialog.Commands.Add(new Windows.UI.Popups.UICommand("Yes"));
                    LogoutDialog.Commands.Add(new Windows.UI.Popups.UICommand("No"));

                    var LogoutResult = await LogoutDialog.ShowAsync();

                    if (LogoutResult.Label == "Yes")
                    {
                        Logout l = new Logout();
                        l.logout(f);
                    }
                    break;

                case "Exit":
                    var ExitDialog = new Windows.UI.Popups.MessageDialog("Are you sure you want to exit Lunch to Go?");

                    ExitDialog.Commands.Add(new Windows.UI.Popups.UICommand("Yes"));
                    ExitDialog.Commands.Add(new Windows.UI.Popups.UICommand("No"));

                    var ExitResult = await ExitDialog.ShowAsync();

                    if (ExitResult.Label == "Yes")
                    {
                        CoreApplication.Exit();
                    }
                    break;
            }
        }
    }
}
