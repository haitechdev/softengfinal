﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template10.Mvvm;

namespace D321_A2_Haitana.ViewModels
{
    /// <summary>
    /// This is the Account Page ViewModel which is used to populate the account page with the users details
    /// This allows the user to look at the information on file and make the required changes
    /// </summary>
    public class AccountPageViewModel: ViewModelBase
    {
        public AccountPageViewModel()
        {

        }

        string _FirstName = App.user.FirstName;
        string _LastName = App.user.LastName;
        string _Email = App.user.Email;
        string _Address = App.user.StreetAddress;
        string _TownCity = App.user.TownCity;
        int _Region = App.user.RegionID;
        string _Phone = App.user.Phone.ToString();
        string _HeaderTitle = "Update Account Details (" + App.user.Username + ")";
        List<string> _Regions = new List<string>()
        {
            "Manawatu",
            "Wanganui",
            "Wairarpa"
        };

        List<string> _Months = new List<string>()
        {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"
        };

        List<string> _Years = new List<string>()
        {
            "16",
            "17",
            "18",
            "19",
            "20"
        };        

        public string FirstName { get { return _FirstName; } set { Set(ref _FirstName, value); } }
        public string LastName { get { return _LastName; } set { Set(ref _LastName, value); } }
        public string Email { get { return _Email; } set { Set(ref _Email, value); } }
        public string Address { get { return _Address; } set { Set(ref _Address, value); } }
        public string TownCity { get { return _TownCity; } set { Set(ref _TownCity, value); } }
        public int Region { get { return _Region; } set { Set(ref _Region, value); } }
        public string Phone { get { return _Phone; } set { Set(ref _Phone, value); } }
        public string HeaderTitle { get { return _HeaderTitle; } set { Set(ref _HeaderTitle, value); } }
        public List<string> Regions { get { return _Regions; } set { Set(ref _Regions, value); } }
        public List<string> Months { get { return _Months; } set { Set(ref _Months, value); } }
        public List<string> Years { get { return _Years; } set { Set(ref _Years, value); } }
    }
}
