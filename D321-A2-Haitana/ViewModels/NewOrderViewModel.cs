﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template10.Mvvm;

namespace D321_A2_Haitana.ViewModels
{
    /// <summary>
    /// This is the ViewModel for the New Order Pages
    /// 
    /// It is used to populates fields with the users information to make it easier for the user e.g. Delivery Details
    /// 
    /// It is also used the populate the Option and Regions comboboxes
    /// </summary>
    public class NewOrderViewModel: ViewModelBase
    {
        public NewOrderViewModel()
        {

        }

        string _OptionText = "Option: " + App.order.Option;
        string _AddOnText = "Choice: " + App.order.AddOn;
        string _DeliveryText = "Delivery Time: " + App.order.Delivery;
        string _Cost ="Total Cost: $10.00";
        string _Address = App.user.StreetAddress;
        string _TownCity = App.user.TownCity;
        int _Region = App.user.RegionID;
        List<string> _Regions = new List<string>()
        {
            "Manawatu",
            "Wanganui",
            "Wairarpa"
        };
        List<string> _Options = new List<string>()
        {
            "Select Option",
            "Beef Noodle Salad",
            "Green Salad Lunch",
            "Lamb Korma",
            "Open Chicken Sandwhich"
        };

        public List<string> Options { get { return _Options; } set { Set(ref _Options, value); } }
        public string Option { get { return _OptionText; } set { Set(ref _OptionText, value); } }
        public string AddOn { get { return _AddOnText; } set { Set(ref _AddOnText, value); } }
        public string DeliveryText { get { return _DeliveryText; } set { Set(ref _DeliveryText, value); } }
        public string Cost { get { return _Cost; } set { Set(ref _Cost, value); } }
        public string Address { get { return _Address; } set { Set(ref _Address, value); } }
        public string TownCity { get { return _TownCity; } set { Set(ref _TownCity, value); } }
        public int Region { get { return _Region; } set { Set(ref _Region, value); } }
        public List<string> Regions { get { return _Regions; } set { Set(ref _Regions, value); } }
    }
}
