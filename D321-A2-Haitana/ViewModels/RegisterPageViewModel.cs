﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template10.Mvvm;

namespace D321_A2_Haitana.ViewModels
{
    /// <summary>
    /// This is the Register View Model page that is used to populate the comboxes on the registration page
    /// </summary>
    public class RegisterPageViewModel: ViewModelBase
    {
        List<string> _Regions = new List<string>()
        {
            "Manawatu",
            "Wanganui",
            "Wairarpa"
        };

        List<string> _Months = new List<string>()
        {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"
        };

        List<string> _Years = new List<string>()
        {
            "16",
            "17",
            "18",
            "19",
            "20"
        };

        public List<string> Regions { get { return _Regions; } set { Set(ref _Regions, value); } }
        public List<string> Months { get { return _Months; } set { Set(ref _Months, value); } }
        public List<string> Years { get { return _Years; } set { Set(ref _Years, value); } }
    }
}
