﻿using D321_A2_Haitana.Models;
using D321_A2_Haitana.Services;
using SQLite.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace D321_A2_Haitana.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RegisterPage : Page
    {
        public RegisterPage()
        {
            this.InitializeComponent();
        }

        //This method is the click event method for the register button.
        //When it is clicked all the data entered by the user is then validated
        //If there are no errors with the validatoin then the users details are inserted into the SQLite database
        //Otherwise the required error messages are displayed
        private async void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            var query = App.conn.Table<Customers>();
            RegisterUser ru = new RegisterUser();
            string result;

            if(TxtUsername.Text != "" && TxtFirstName.Text != "" && TxtLastName.Text != "" && TxtEmail.Text != "" && TxtAddr.Text != "" && TxtTownCity.Text != "" && cbxRegion.SelectedItem.ToString() != "" && TxtPhNo.Text != "" && TxtPwd.Password != "" && TxtConfirmPwd.Password != "" && TxtCCNo.Text != "" && cbxMonth.SelectedItem.ToString() != "" && cbxYear.SelectedItem.ToString() != "" && TxtCVC.Text != "")
            {
                App.conn.CreateTable<Customers>();
                bool unameUsed = false;

                foreach (var user in query)
                {
                    if (user.Username == TxtUsername.Text)
                    {
                        unameUsed = true;
                    }
                }
                if(unameUsed == false)
                {
                    result = await ru.Register(TxtUsername.Text, TxtFirstName.Text, TxtLastName.Text, TxtEmail.Text, TxtAddr.Text, TxtTownCity.Text, cbxRegion.SelectedItem.ToString(), TxtPhNo.Text, TxtPwd.Password, TxtConfirmPwd.Password, TxtCCNo.Text, cbxMonth.SelectedItem.ToString(), cbxYear.SelectedItem.ToString(), TxtCVC.Text);
                }                
            }
            else
            {
                App.ErrorMessage = "Fill in all the feilds!!!";
            }            

            if(App.ErrorMessage == "")
            {
                Busy.SetBusy(true, "Registration Successfull!");

                var s = App.conn.Insert(new Customers()
                {
                    Username = App.user.Username,
                    FirstName = App.user.FirstName,
                    LastName = App.user.LastName,
                    Email = App.user.Email,
                    StreetAddress = App.user.StreetAddress,
                    TownCity = App.user.TownCity,
                    Region = App.user.Region,
                    RegionID = App.user.RegionID,
                    Phone = App.user.Phone,
                    Pin = App.user.Pin,
                    CreditCardNo = App.user.CreditCardNo,
                    CreditCardExpM = cbxMonth.SelectedItem.ToString(),
                    CreditCardExpY = cbxYear.SelectedItem.ToString(),
                    CreditCardCvc = App.user.CreditCardCvc
                });

                await Task.Delay(1000);                
                Frame.Navigate(typeof(LoginPage));
                Busy.SetBusy(false);
            }
            else
            {
                ErrorText.Text = App.ErrorMessage;
                App.ErrorMessage = "";
            }
        }
    }
}
