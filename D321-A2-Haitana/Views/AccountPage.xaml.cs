﻿using D321_A2_Haitana.Services;
using D321_A2_Haitana.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace D321_A2_Haitana.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AccountPage : Page
    {
        public AccountPage()
        {
            this.InitializeComponent();
        }


        //Update Users Account Details
        //
        //Check to see if anychanges were made to the form.
        //If there are changes those changes are then validated and updated if they are valid
        private async void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            Busy.SetBusy(true, "Updating Account Details...");
            await Task.Delay(1000);

            var selectedUser = App.conn.Query<Customers>("select * from Customers where Username = ?", App.user.Username).FirstOrDefault();

            bool[] update = new bool[9];
            update[0] = true;
            update[1] = true;
            update[2] = true;
            update[3] = true;
            update[4] = true;
            update[5] = true;
            update[6] = true;
            update[7] = true;
            update[8] = true;

            ErrorText.Text = "";

            if (TxtPwd.Password != "" && TxtConfirmPwd.Password != "")
            {
                if (TxtPwd.Password == TxtConfirmPwd.Password)
                {
                    selectedUser.Pin = TxtPwd.Password;
                    update[0] = true;
                }
                else
                {
                    ErrorText.Text = ErrorText.Text + "Passwords don't match!\n";
                    update[0] = false;
                }
            }

            if (TxtFirstName.Text != "")
            {
                selectedUser.FirstName = TxtFirstName.Text;
                update[1] = true;
            }
            else
            {
                ErrorText.Text = ErrorText.Text + "First Name Cannot be blank!\n";
                update[1] = false;
            }

            if (TxtLastName.Text != "")
            {
                selectedUser.LastName = TxtLastName.Text;
                update[2] = true;
            }
            else
            {
                ErrorText.Text = ErrorText.Text + "Last Name Cannot be blank!\n";
                update[2] = false;
            }

            if (TxtEmail.Text != "")
            {
                selectedUser.Email = TxtEmail.Text;
                update[3] = true;
            }
            else
            {
                ErrorText.Text = ErrorText.Text + "Email Cannot be blank!\n";
                update[3] = false;
            }

            if (TxtAddr.Text != "")
            {
                selectedUser.StreetAddress = TxtAddr.Text;
                update[4] = true;
            }
            else
            {
                ErrorText.Text = ErrorText.Text + "Street Address Cannot be blank!\n";
                update[4] = false;
            }

            if (TxtTownCity.Text != "")
            {
                selectedUser.TownCity = TxtTownCity.Text;
                update[5] = true;
            }
            else
            {
                ErrorText.Text = ErrorText.Text + "Town/City Cannot be blank!\n";
                update[5] = false;
            }

            if (cbxRegion.SelectedIndex != -1)
            {
                selectedUser.Region = cbxRegion.SelectedItem.ToString();
                selectedUser.RegionID = cbxRegion.SelectedIndex;
                update[6] = true;
            }
            else
            {
                ErrorText.Text = ErrorText.Text + "Region Cannot be blank!\n";
                update[6] = false;
            }

            if (TxtPhNo.Text != "")
            {
                try
                {
                    selectedUser.Phone = Convert.ToInt32(TxtPhNo.Text);
                    update[7] = true;
                }
                catch
                {
                    ErrorText.Text = ErrorText.Text + "Phone Number can only be Numbers!\n";
                    update[7] = false;
                }
            }
            else
            {
                ErrorText.Text = ErrorText.Text + "Phone Cannot be blank!\n";
                update[7] = false;
            }

            if (TxtCCNo.Text != "" || cbxMonth.SelectedIndex != -1 || cbxYear.SelectedIndex != -1 || TxtCVC.Text != "")
            {
                bool ccStatus = true;
                Services.Validation v = new Services.Validation();
                v.CreditCardValidator();
                bool result = v.HasCreditCardNumber(TxtCCNo.Text);

                if (result == true)
                {
                    selectedUser.CreditCardNo = Convert.ToInt64(TxtCCNo.Text);
                }
                else
                {
                    ErrorText.Text = ErrorText.Text + "You need to input a vaild credit card number!\n";
                    ccStatus = false;
                }

                try
                {
                    selectedUser.CreditCardExpM = cbxMonth.SelectedItem.ToString();
                }
                catch
                {
                    ErrorText.Text = ErrorText.Text + "You need to input a vaild credit expiry month!\n";
                    ccStatus = false;
                }

                try
                {
                    selectedUser.CreditCardExpY = cbxYear.SelectedItem.ToString();
                }
                catch
                {
                    ErrorText.Text = ErrorText.Text + "You need to input a vaild credit expiry year!\n";
                    ccStatus = false;
                }

                try
                {
                    if (TxtCVC.Text.Length == 3)
                    {
                        selectedUser.CreditCardCvc = Convert.ToInt32(TxtCVC.Text);
                    }
                    else
                    {
                        ErrorText.Text = ErrorText.Text + "CVC Number must be 3 digits!\n";
                        ccStatus = false;
                    }
                }
                catch
                {
                    ErrorText.Text = ErrorText.Text + "CVC must be Numbers!\n";
                    ccStatus = false;
                }

                if (ccStatus == true)
                {
                    update[8] = true;
                }
                else
                {
                    update[8] = false;
                }
            }

            bool goUpdate = true;

            foreach(bool u in update)
            {
                if(u == false)
                {
                    goUpdate = false;
                    break;
                }
            }

            if(goUpdate == true)
            {
                App.conn.RunInTransaction(() =>
                {
                    App.conn.Update(selectedUser);
                });
                Busy.SetBusy(true, "Account details updated successfully!");
                await Task.Delay(1000);
                ErrorText.Text = "";
                App.user = selectedUser;
            }
            
            Busy.SetBusy(false);
        }

        //This method is called when the user clicks the delete account button
        //They are asked if they are sure they want to delete their account
        //If they click yes then the delete user method is called
        private async void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new Windows.UI.Popups.MessageDialog("Are you sure you want to delete your account?");

            dialog.Commands.Add(new Windows.UI.Popups.UICommand("Yes"));
            dialog.Commands.Add(new Windows.UI.Popups.UICommand("No"));

            var DeleteResult = await dialog.ShowAsync();

            if (DeleteResult.Label == "Yes")
            {
                DeleteCustomer d = new DeleteCustomer();
                d.DeleteUser(App.user.Username, Frame);
            }
        }
    }
}
