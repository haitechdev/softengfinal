﻿using D321_A2_Haitana.ViewModels;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Controls;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using D321_A2_Haitana.Services;
using Template10.Services.NavigationService;
using Windows.ApplicationModel.Core;

namespace D321_A2_Haitana.Views
{
    public sealed partial class NewOrderPage : Page
    {
        public NewOrderPage()
        {
            InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Enabled;

            checkOrderTime();
        }

        //Navigates to the confirm new order page if all the options are selected
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if ((App.order.Option != null || App.order.Option != "Select Option") && App.order.AddOn != null && App.order.Delivery != null)
            {
                Error.Text = "";
                Frame.Navigate(typeof(NewOrderPage2));
            }
            else
            {
                Error.Text = "Ensure all options are selected!";
            }
        }

        //When are option is selected the correct AddOn Choices are displayed
        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;

            App.order.Option = comboBox.SelectedItem as string;
            App.order.OptionID = comboBox.SelectedIndex;

            if (App.order.Option == "Beef Noodle Salad")
            {
                Flakes.Visibility = Visibility.Visible;
                Spice.Visibility = Visibility.Collapsed;
                Dressing.Visibility = Visibility.Collapsed;
                Bread.Visibility = Visibility.Collapsed;
            }
            if (App.order.Option == "Green Salad Lunch")
            {
                Dressing.Visibility = Visibility.Visible;
                Spice.Visibility = Visibility.Collapsed;
                Flakes.Visibility = Visibility.Collapsed;
                Bread.Visibility = Visibility.Collapsed;

            }
            if (App.order.Option == "Lamb Korma")
            {
                Spice.Visibility = Visibility.Visible;
                Dressing.Visibility = Visibility.Collapsed;
                Flakes.Visibility = Visibility.Collapsed;
                Bread.Visibility = Visibility.Collapsed;
            }
            if (App.order.Option == "Open Chicken Sandwhich")
            {
                Bread.Visibility = Visibility.Visible;
                Spice.Visibility = Visibility.Collapsed;
                Flakes.Visibility = Visibility.Collapsed;
                Dressing.Visibility = Visibility.Collapsed;
            }
            if (App.order.Option == "Select Option")
            {
                Bread.Visibility = Visibility.Collapsed;
                Spice.Visibility = Visibility.Collapsed;
                Flakes.Visibility = Visibility.Collapsed;
                Dressing.Visibility = Visibility.Collapsed;
            }
        }

        private void DeliveryRadioButton_Click(object sender, RoutedEventArgs e)
        {
            var radioButton = sender as RadioButton;
            App.order.Delivery = radioButton.Content.ToString();
        }

        private void AddRadioButton_Click(object sender, RoutedEventArgs e)
        {
            var radioButton = sender as RadioButton;

            App.order.AddOn = radioButton.Content.ToString();
        }

        //Checks to see if the time is before the order cutoff time
        private void checkOrderTime()
        {
            Services.Validation v = new Services.Validation();
            int canOrder = v.CheckOrderTime();

            if (canOrder < 0)
            {
                OrderDateTblk.Text = "Order Date and Time: " + App.order.DateTime;
            }
            else
            {
                OrderDateTblk.Text = "Sorry, orders for today have closed!";
                comboBox.IsEnabled = false;
                RanchRad.IsEnabled = false;
                VinaigretteRad.IsEnabled = false;
                NoneRad.IsEnabled = false;
                DeliveryOneRad.IsEnabled = false;
                DeliveryTwoRad.IsEnabled = false;
                DeliveryThreeRad.IsEnabled = false;
                DeliveryFourRad.IsEnabled = false;
                btnNext.IsEnabled = false;
            }
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            var o = sender as AppBarButton;

            DialogClass d = new DialogClass();

            d.ShowDialog(o, Frame);
        }
    }
}