﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace D321_A2_Haitana.Views
{
    /// <summary>
    /// This page is shown when the user open the app with out an internet connection
    /// </summary>
    public sealed partial class NoInternetPage : Page
    {
        public NoInternetPage()
        {
            this.InitializeComponent();
        }

        private void RetryButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(LoadAppPage));
        }
    }
}
