﻿using D321_A2_Haitana.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace D321_A2_Haitana.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CurrentOrdersPage : Page
    {
        private List<Orders> Orders;
        private Orders order;
        private bool OrderReceivedStatus = true;

        public CurrentOrdersPage()
        {
            this.InitializeComponent();
            //This retieves all the users current orders
            Orders = UsersOrders.RetrieveOrders(App.user.Username, false);

            foreach(Orders o in Orders)
            {
                if(o.Received == false)
                {
                    RetryOrdersStatus.Text = "Not all orders have been sent!";
                    RetryAllOrders.Visibility = Visibility.Visible;
                    OrderReceivedStatus = false;
                    break;
                }
            }
            //If the are no current orders then a message is displayed
            if (Orders.Count == 0)
            {
                NoCurrentOrders.Visibility = Visibility.Visible;
            }
            else
            {
                NoCurrentOrders.Visibility = Visibility.Collapsed;
            }
        }

        //When a current order i selected the details for that order are stored in an Orders object
        //Then the appropriate button are shown
        private void GridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            order = (Orders)e.ClickedItem;

            if(order.Received == false)
            {
                RetryOrder.Visibility = Visibility.Visible;
                OrderReceived.Visibility = Visibility.Collapsed;
            }
            else
            {
                RetryOrder.Visibility = Visibility.Collapsed;
                OrderReceived.Visibility = Visibility.Visible;
            }
        }

        //Sets the order status to true. so the order now shows on the past orders page
        private void OrderReceived_Click(object sender, RoutedEventArgs e)
        {
            var selectedOrder = App.conn.Query<Orders>("select * from Orders where Id = ?", order.Id).FirstOrDefault();

            if (selectedOrder != null)
            {
                selectedOrder.Status = true;
                App.conn.RunInTransaction(() =>
                {
                    App.conn.Update(selectedOrder);
                });
                
                Frame.Navigate(typeof(CurrentOrdersPage));
                Frame.GoBack();
            }
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            var o = sender as AppBarButton;

            DialogClass d = new DialogClass();

            d.ShowDialog(o, Frame);
        }        

        //tries to resend the selected order
        private async void RetryOrder_Click(object sender, RoutedEventArgs e)
        {
            ResendOrders ro = new ResendOrders();
            await ro.SingleLocalOrder(order);
            Busy.SetBusy(false);
            Frame.Navigate(typeof(CurrentOrdersPage));
            Frame.GoBack();
        }


        //Runs the resend all orders method
        private async void RetryAllOrders_Click(object sender, RoutedEventArgs e)
        {
            ResendOrders ro = new ResendOrders();
            await ro.AllLocalOrders(App.user.Username);
        }
    }
}
