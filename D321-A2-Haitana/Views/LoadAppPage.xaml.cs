﻿using D321_A2_Haitana.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace D321_A2_Haitana.Views
{
    public sealed partial class LoadAppPage : Page
    {
        public LoadAppPage()
        {
            this.InitializeComponent();
            Shell.HamburgerMenu.IsFullScreen = true;
            LoadingApp();
        }

        //This method is used when the app is first loaded
        //It checks that there is a valid internet connection, then checks if the user is logged in.
        //If they are looged in the their orders are loaded and taken to the Current Orders page - If there are no orders then they are taken to the New Order page
        //If they weren't logged in then then are taken to the login page
        public async void LoadingApp()
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile userFile;
            try
            {
                userFile = await storageFolder.GetFileAsync("loggedIn.txt");
            }
            catch
            {
                userFile = await storageFolder.CreateFileAsync("loggedIn.txt");
            }

            string username = "";
            bool hasInternet = false;
            NetworkConnectionTrigger network = new NetworkConnectionTrigger();

            Busy.SetBusy(true, "Checking Internet Connection...");
            hasInternet = network.getStatus();
            await Task.Delay(1500);

            if (hasInternet == true)
            {
                Busy.SetBusy(true, "Checking Login Status...");
                username = await FileIO.ReadTextAsync(userFile);
                await Task.Delay(1500);

                if (username != "")
                {
                    Busy.SetBusy(true, "Loading your Orders...");
                    App.user = GetUser.RetrieveUser(username);
                    App.orders = UsersOrders.RetrieveOrders(App.user.Username, false);
                    await Task.Delay(1500);

                    if (App.orders.Count > 0)
                    {
                        ResendOrders ro = new ResendOrders();
                        await ro.AllLocalOrders(App.user.Username);
                        Shell.HamburgerMenu.IsFullScreen = false;
                        Frame.Navigate(typeof(CurrentOrdersPage));
                    }
                    else
                    {
                        Busy.SetBusy(false);
                        Shell.HamburgerMenu.IsFullScreen = false;
                        Frame.Navigate(typeof(NewOrderPage));
                    }
                }
                else
                {
                    Busy.SetBusy(false);
                    Frame.Navigate(typeof(LoginPage));
                }
            }
            else
            {
                Busy.SetBusy(false);
                Frame.Navigate(typeof(NoInternetPage));
            }
            Busy.SetBusy(false);
        }
    }
}
