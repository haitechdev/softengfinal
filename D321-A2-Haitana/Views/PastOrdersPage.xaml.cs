﻿using D321_A2_Haitana.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using static Template10.Common.BootStrapper;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace D321_A2_Haitana.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PastOrdersPage : Page
    {
        private List<Orders> Orders;
        private Orders order;

        public PastOrdersPage()
        {
            InitializeComponent();

            //This retieves all the users past orders
            Orders = UsersOrders.RetrieveOrders(App.user.Username, true);

            //If the are no past orders then a message is displayed
            if (Orders.Count == 0)
            {
                NoPastOrders.Visibility = Visibility.Visible;
            }
            else
            {
                NoPastOrders.Visibility = Visibility.Collapsed;
            }
        }

        //This method display a popup dialog asking if the user wants to delete the past order
        //If the user clicks yes - it then deletes the selected past order from the SQLite database
        private async void DeleteOrder_Click(object sender, RoutedEventArgs e)
        {
            var CancelDialog = new Windows.UI.Popups.MessageDialog("Are you sure you want to delete this order?");

            CancelDialog.Commands.Add(new Windows.UI.Popups.UICommand("Yes"));
            CancelDialog.Commands.Add(new Windows.UI.Popups.UICommand("No"));

            var CancelResult = await CancelDialog.ShowAsync();

            if (CancelResult.Label == "Yes")
            {
                App.conn.Delete(order);
                Frame.Navigate(typeof(PastOrdersPage));
                Frame.GoBack();
            }
        }

        //When a past order i selected the details for that order are stored in an Orders object
        //Then the appropriate button are shown
        private void GridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            order = (Orders)e.ClickedItem;
            DeleteOrder.Visibility = Visibility.Visible;
            Reorder.Visibility = Visibility.Visible;
        }

        //Calls the ShowDilog method to display the appropriate popup dialog
        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            var o = sender as AppBarButton;
            DialogClass d = new DialogClass();
            d.ShowDialog(o, Frame);
        }

        //Sets App.order to the order selected the navigates to the New Order Page
        private void Reorder_Click(object sender, RoutedEventArgs e)
        {
            App.order = order;
            Frame.Navigate(typeof(NewOrderPage));
        }
    }
}
