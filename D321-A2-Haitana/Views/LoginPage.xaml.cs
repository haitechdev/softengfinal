﻿using D321_A2_Haitana.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace D321_A2_Haitana.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginPage : Page
    {
        //Hide the hamburger menu
        public LoginPage()
        {
            this.InitializeComponent();
            Shell.HamburgerMenu.IsFullScreen = true;

        }

        //When the login button is clicked the details are checked against the SQLite database.
        //If the logon was sucessful then the user orders are retrieved and they are taken to the Current Orders page.
        //If there are no current orders then they are taken to the new order page
        private async void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            Busy.SetBusy(true, "Checking Login Details...");
            await Task.Delay(1000);
            CheckLoginDetails cld = new CheckLoginDetails();
            bool successfullLogin;

            successfullLogin = cld.CheckUserDetails(UsernameTextBox.Text, PinTextBox.Password);
            

            if (successfullLogin == true)
            {
                Shell.HamburgerMenu.IsFullScreen = false;

                Busy.SetBusy(true, "Retrieving your orders...");
                await Task.Delay(1000);
                List<Orders> O = UsersOrders.RetrieveOrders(App.user.Username, false);

                cld.savedUser(App.user.Username);

                await Task.Delay(1000);
                LoadAppPage(O);

                Busy.SetBusy(false);
            }
            else
            {
                Busy.SetBusy(false);
                ErrorMessage.Text = "Incorrect Login Details!";
            }
        }

        //Navigates to the registration page
        private void RegisterButtonTextBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(RegisterPage));
        }

        private void LoadAppPage(List<Orders> O)
        {
            if (O.Count > 0)
            {
                Frame.Navigate(typeof(CurrentOrdersPage));
            }
            else
            {
                Frame.Navigate(typeof(NewOrderPage));
            }
        }
    }
}
