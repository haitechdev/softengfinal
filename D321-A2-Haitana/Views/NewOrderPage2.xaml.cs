﻿using D321_A2_Haitana.Services;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace D321_A2_Haitana.Views
{
    public sealed partial class NewOrderPage2 : Page
    {
        public NewOrderPage2()
        {
            this.InitializeComponent();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(NewOrderPage));
        }

        private void tbxTownCity_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            App.order.TownCity = tbxTownCity.Text;
        }

        private void tbxStreetAddress_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            App.order.StreetAddress = tbxStreetAddress.Text;
        }

        private void cbxRegion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;

            App.order.Region = comboBox.SelectedItem as string;
            App.order.RegionID = comboBox.SelectedIndex;
        }

        //This method is used to send thet users order to the Food to Go Ordering System
        //It Checks that theres internet
        //If there is the order is then POSTed using HttpClient to the Food to Go Insert API
        //A response is then received saying wheather the order was stored on the Ordering System or not
        //If it failed to store then the received value is set to false
        //Then the order is store locally on the SQLite database
        private async void btnSend_Click(object sender, RoutedEventArgs e)
        {
            bool hasInternet = false;
            NetworkConnectionTrigger network = new NetworkConnectionTrigger();

            Busy.SetBusy(true, "Checking Internet Connection...");
            hasInternet = network.getStatus();
            await Task.Delay(500);

            Busy.SetBusy(true, "Processing your order...");
            await Task.Delay(500);
            if (tbxStreetAddress.Text != "" && tbxTownCity.Text != "" && cbxRegion.SelectedItem != null)
            {
                App.order.StreetAddress = tbxStreetAddress.Text;
                App.order.TownCity = tbxTownCity.Text;
                App.order.Region = cbxRegion.SelectedItem.ToString();
                App.order.RegionID = cbxRegion.SelectedIndex;
                App.order.Customer = App.user.Username;
                App.order.Cost = "$10.00";
                App.order.Status = false;

                if(hasInternet == true)
                {
                    HTTPClient post = new HTTPClient();
                    await post.InsertOrder(App.order);
                    insertOrderLocally(App.OrderReceieved);
                }
                else
                {
                    insertOrderLocally(false);
                    Busy.SetBusy(true, "Could not connect with Food to Go's ordering system...");
                    await Task.Delay(1000);
                    Busy.SetBusy(true, "Order has been saved locally...");
                    await Task.Delay(1000);
                    Busy.SetBusy(false);
                }
                await Task.Delay(1000);                 
            }
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            var o = sender as AppBarButton;

            DialogClass d = new DialogClass();

            d.ShowDialog(o, Frame);
        }

        //Inserts the order into the SQLite database then navigates to the Current Orders Page
        public void insertOrderLocally(bool r)
        {
            App.conn.Insert(new Orders()
            {
                OptionID = App.order.OptionID,
                Option = App.order.Option,
                AddOn = App.order.AddOn,
                Delivery = App.order.Delivery,
                StreetAddress = App.order.StreetAddress,
                TownCity = App.order.TownCity,
                Region = App.order.Region,
                RegionID = App.order.RegionID,
                DateTime = App.order.DateTime,
                Cost = App.order.Cost,
                Customer = App.order.Customer,
                Status = false,
                Received = r
            });
            App.order = new Orders();

            Frame.Navigate(typeof(CurrentOrdersPage));
        }
    }
}
