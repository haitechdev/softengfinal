﻿using D321_A2_Haitana;
using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D321_A2_Haitana.Services
{
    public class UsersOrders
    {
        //Returns a Customers Orders from the SQLite database - in a List based on there user name and the status of the order.
        //status = true will return Past/Completed orders
        //status = false will return Current/Uncompleted orders
        public static List<Orders> RetrieveOrders(string uname, bool status)
        {
            var o = App.conn.Query<Orders>("select * from Orders where Customer = ? and Status = ?", uname, status);

            return o;
        }
    }

    //The Orders class - used within the app for creating Orders objects and for creating the SQLite database 
    public class Orders
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Option { get; set; }
        public int OptionID { get; set; }
        public string AddOn { get; set; }
        public string Delivery { get; set; }
        public string StreetAddress { get; set; }
        public string TownCity { get; set; }
        public string Region { get; set; }
        public int RegionID { get; set; }
        public DateTime DateTime { get; set; }
        public string Cost { get; set; }
        public string Customer { get; set; }
        public bool Status { get; set; }
        public bool Received { get; set; }
    }
}
