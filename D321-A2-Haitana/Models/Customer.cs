﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D321_A2_Haitana.Services
{
    //The Customers object - used within the app to create Customer objects and for creating the SQLite database
    public class Customers
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Phone { get; set; }
        public string StreetAddress { get; set; }
        public string TownCity { get; set; }
        public string Region { get; set; }
        public int RegionID { get; set; }
        public long CreditCardNo { get; set; }
        public string CreditCardExpM { get; set; }
        public string CreditCardExpY { get; set; }
        public int CreditCardCvc { get; set; }
        public string Username { get; set; }
        public string Pin { get; set; }
    }
}
