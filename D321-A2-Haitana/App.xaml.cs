using Windows.UI.Xaml;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Template10.Controls;
using Template10.Common;
using System;
using System.Linq;
using Windows.UI.Xaml.Data;
using System.Collections.Generic;
using SQLite.Net;
using D321_A2_Haitana.Services;
using Windows.Storage;
using System.IO;
using D321_A2_Haitana.Views;
using D321_A2_Haitana.Services.SettingsServices;

namespace D321_A2_Haitana
{
    /// Documentation on APIs used in this page:
    /// https://github.com/Windows-XAML/Template10/wiki

    [Bindable]
    sealed partial class App : Template10.Common.BootStrapper
    {
        public static Orders order = new Orders();
        public static List<Orders> orders = new List<Orders>();
        public static Customers user = new Customers();        
        public static bool OrderReceieved;
        public static string ErrorMessage = "";
        public static string SQLiteFilename = "db.sqlite";
        public static string path = Path.Combine(ApplicationData.Current.LocalFolder.Path, SQLiteFilename);
        public static SQLiteConnection conn = new SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path);

        public App()
        {
            InitializeComponent();
            SplashFactory = (e) => new Views.Splash(e);

            conn.CreateTable<Customers>();
            conn.CreateTable<Orders>();

            #region App settings

            var _settings = SettingsService.Instance;
            RequestedTheme = _settings.AppTheme;
            CacheMaxDuration = _settings.CacheMaxDuration;
            ShowShellBackButton = _settings.UseShellBackButton;

            #endregion
        }

        public override async Task OnInitializeAsync(IActivatedEventArgs args)
        {
            if (Window.Current.Content as ModalDialog == null)
            {
                // create a new frame 
                var nav = NavigationServiceFactory(BackButton.Attach, ExistingContent.Include);

                // create modal root
                Window.Current.Content = new ModalDialog
                {
                    DisableBackButtonWhenModal = true,
                    Content = new Views.Shell(nav),
                    ModalContent = new Views.Busy(),
                    MinHeight = 400,
                };
            }

            await Task.CompletedTask;
        }

        public override async Task OnStartAsync(StartKind startKind, IActivatedEventArgs args)
        {
            // long-running startup tasks go here
            //await Task.Delay(5000);

            NavigationService.Navigate(typeof(Views.LoadAppPage));
            await Task.CompletedTask;
        }
    }
}

